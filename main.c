#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "moduly.h"


int main()
{
    int number_from_user;
    struct Client *head;
    head = (struct Client*)malloc(sizeof(struct Client));
    strcpy(head -> surname, "Nowak");
    head->next = NULL;
    char new_surname[20]= "";

    //petla zewnetrza, dopoki nie zostanie wybrana opcja 2 lista jest caly czas modyfikowana
    int running = 1;
    while(running)
    {    
        while(number_from_user<1 || number_from_user>5) //petla sprawdzajaca czy uzytkownik podal liczbe z przedzialu od 1 do 5, jesli nie to podaje jeszcze raz
        {
            printf("Podaj liczbe od 1 do 5 w zaleznosci od tego co chcesz zrobic:\n1.Dodaj nowego klienta\n2.Usun danego klienta\n3.Wyswietl ilosc klientow\n4.Wyswietl nazwiska klientow\n5.Zakoncz modyfikacje\nLiczba: ");
            scanf("%d", &number_from_user);
        }
            switch (number_from_user)
            {
            case 1:
                printf("Podaj nazwisko nowego klienta:");
                scanf("%s", new_surname);
                push_back(&head, new_surname);
                break;
            case 2:
                new_surname[0] = '\0';
                printf("Podaj nazwisko klienta do usuniecia: ");
                scanf("%s", new_surname);
                pop_by_surname(&head, new_surname);
                break;
            case 3:
                printf("Ilosc klientow: %d\n\n", list_size(head));
                break;
            case 4:
                show_list(head);
                break;

            case 5:
                running = 0;
            }

    number_from_user = 0;
    }
    

    free(head);
    return 0;
}