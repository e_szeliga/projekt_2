CC = gcc
CFLAGS = -Wall
LIBS = -lm

Projekt_2: main.o klient.o 
	gcc -o Projekt_2 main.o klient.o -lm 

main.o: main.c moduly.h
	gcc -c main.c

klient.o: klient.c
	gcc -c klient.c

clean:
	rm main.o
	rm klient.o
	rm Projekt_2.exe